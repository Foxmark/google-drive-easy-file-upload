<?php
namespace Foxmark;

use Google_Client as Google_Client;
use Google_Service_Drive as Google_Service_Drive;
use Google_Service_Drive_DriveFile as Google_Service_Drive_DriveFile;
use PHPUnit\Runner\Exception;

class EasyGdrive
{
    private $config  = [
        'app_name' => 'Easy GDrive',
        'credentials_file' => 'credentials.json',
        'credentials_dir'  => 'config/',
        'token_file'       => 'token.json',
        'token_dir'        => 'tokens',
        'scopes'           => [
            Google_Service_Drive::DRIVE_FILE,
            Google_Service_Drive::DRIVE_METADATA_READONLY
        ],
        'access_type'       => 'offline'
    ];
    private $client  = false;
    private $service = false;
    public $verbose  = true;

    public function __construct(array $config = [])
    {
        $new_config =  array_replace($this->config, $config);
        if($new_config) {
            $this->config = $new_config;
        }
        $this->client  = $this->getGoogleClient();
        $this->service = $this->getGoogleService();

    }

    private function getGoogleService()
    {
        if(!$this->client){
            throw new Exception('Google client not available');
        }
        return new Google_Service_Drive($this->client);
    }

    private function getGoogleClient()
    {
        // based on https://developers.google.com/drive/api/v3/quickstart/php
        $client = new Google_Client();
        $client->setApplicationName($this->config['app_name']);
        $client->setScopes($this->config['scopes']);
        $client->setAuthConfig($this->escapeDir($this->config['credentials_dir']) . $this->config['credentials_file']);
        $client->setAccessType($this->config['access_type']);
        $client->setPrompt('select_account consent');

        $tokenPath = $this->escapeDir($this->config['token_dir']) . $this->config['token_file'];
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                $this->createNewToken($client, $tokenPath);
            }
        }
        return $client;
    }

    public function createNewToken(Google_Client $client, string $tokenPath) {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
        $client->setAccessToken($accessToken);

        // Check to see if there was an error.
        if (array_key_exists('error', $accessToken)) {
            throw new Exception(join(', ', $accessToken));
        }

        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }

    public function getClient() {
        return $this->client;
    }

    public function escapeDir($directory) {
        if(empty($directory)){
            return '';
        }
        return rtrim($directory, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }

    public function newFolder(string $folder_name, int $parentID = 0) {
        $lines    = explode('/', $folder_name);
        $results  = [];
        foreach ($lines as $folder) {
            if(empty($folder)) {
                continue;
            }
            $config = [
                'name' => $folder,
                'mimeType' => 'application/vnd.google-apps.folder'
            ];
            if($parentID) {
                $config['parents'] = [$parentID];
            }
            $fileMetadata = new Google_Service_Drive_DriveFile($config);
            $result   = $this->service->files->create($fileMetadata, ['fields' => 'id']);
            if($result) {
                $results[] = $result;
                $parentID = $result->id;
            }
            $this->logger("Folder $folder [$parentID] created");
        }
        return $results;
    }

    public function newFile(string $file_path, string $file_name, int $parentID = 0, $verify = false)
    {
        $config = [
            'name' => $file_name
        ];

        if($parentID){
            $config['parents'] = [$parentID];
        }

        $fileMetadata = new Google_Service_Drive_DriveFile($config);

        if(!file_exists($file_path)){
            throw new Exception('File '.$file_path.' not found/unable to read');
        }

        if($verify) {
            $local_md5 = md5_file($file_path);
        }

        $content = file_get_contents($file_path);
        $file = $this->service->files->create($fileMetadata, [
            'data' => $content,
            'uploadType' => 'multipart',
            'fields' => 'id,name,mimeType,parents,webContentLink,size,md5Checksum'
            ]
        );
        $this->logger(printf("File ID: %s\n", $file->id));
        $this->logger(printf("Name: %s\n", $file->name));
        $this->logger(printf("MimeType: %s\n", $file->mimeType));
        $this->logger(printf("Parents: %s\n", implode(', ',$file->parents)));
        $this->logger(printf("WebContentLink: %s\n", $file->webContentLink));
        $this->logger(printf("Size: %s\n", $file->size));
        $this->logger(printf("MD5Checksum: %s\n", $file->md5Checksum));
        if($verify && $local_md5 !== $file->md5Checksum){
            throw new Exception('MD5 check field');
        }
        return $file;
    }

    public function logger($str)
    {
        if($this->verbose) {
            fwrite(STDOUT, $str . PHP_EOL);
        }
    }
}