<?php
require dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'vendor'. DIRECTORY_SEPARATOR . 'autoload.php';

use PHPUnit\Framework\TestCase as TestCase;

class TestSuite1 extends TestCase {
    public function test_instance()
    {
        $ed = new Foxmark\EasyGdrive();
        $this->assertInstanceOf('Foxmark\EasyGdrive',$ed, 'Not an instance of EasyGdrive');
    }

    public function test_getClient()
    {
        $ed = new Foxmark\EasyGdrive();
        $client = $ed->getClient();
        $this->assertInstanceOf('Google_Client',$client, 'Not an instance of Google_Client');
    }

    public function test_escapeDir()
    {
        $ed  = new Foxmark\EasyGdrive();
        $str = $ed->escapeDir('');
        $this->assertEquals('', $str, 'escapeDir does not return empty string');

        $str = $ed->escapeDir('/folder/subfolder');
        $this->assertEquals('/folder/subfolder/', $str, 'escapeDir does not escape directory');
    }

    public function test_newFolder()
    {
        $ed  = new Foxmark\EasyGdrive();
        $ar  = $ed->newFolder('/test_folder/test_subfolder/');
        $this->assertTrue(is_array($ar), 'newFolder does not return array');
        $this->assertTrue(count($ar) == 2, 'newFolder does not return array');
    }

    public function test_newFile()
    {
        $ed  = new Foxmark\EasyGdrive();
        $obj = $ed->newFile(__DIR__ . '/test_image.png', 'test_image.png');
        $this->assertInstanceOf('Google_Service_Drive_DriveFile', $obj, 'incorrect value return by newFile()');
    }
}